# NooLite MTRF MQTT

Ретранслятор сообщений с последовательного порта MTRF в MQTT сообщения

## Установка

Для установки проекта нужен Python 3.5+ и pip

### Из исходников

```bash
# Клонируем репозиторий
$ git clone https://bitbucket.org/ragruslan/noolite-mtrf-to-mqtt

# Заходим в созданную папку репозитория
$ cd noolite-mtrf-to-mqtt

# Устанавливаем сервер
$ pip3 install -r requirements.txt && pip3 install -e . 
```

## Запуск

```
$ noolite_mtrf_mqtt
```

## Работа

MQTT топики для работы:
- noolite/mtrf/send - топик для отправки сообщений на адаптер
- noolite/mtrf/receive - топик, куда публикуются все принятые сообщения с адаптера
