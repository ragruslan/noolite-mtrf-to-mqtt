FROM alpine:latest

MAINTAINER Ruslan Ragimov

ENV mtrf_serial_port_env=/dev/tty.mtrf_serial_port \
    mqtt_scheme=mqtt \
    mqtt_host=127.0.0.1 \
    mqtt_port=__EMPTY__ \
    mqtt_user=__EMPTY__ \
    mqtt_password=__EMPTY__ \
    commands_delay=0.2 \
    logging_level=INFO


RUN mkdir /root/noolite-mtrf-to-mqtt
COPY nmd /root/noolite-mtrf-to-mqtt/nmd
COPY setup.py requirements.txt MANIFEST.in README.md /root/noolite-mtrf-to-mqtt/
RUN apk --update add python3 git && \
    cd /root/noolite-mtrf-to-mqtt && pip3 install -r requirements.txt && pip3 install -e . && \
    apk del git && rm -rf /var/cache/apk/*

CMD noolite_mtrf_mqtt \
        --mtrf-serial-port=$mtrf_serial_port_env \
        --mqtt-scheme=$mqtt_scheme \
        --mqtt-host=$mqtt_host \
        --mqtt-port=$mqtt_port \
        --mqtt-user=$mqtt_user \
        --mqtt-password=$mqtt_password \
        --commands-delay=$commands_delay \
        --logging-level=$logging_level
